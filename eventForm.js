'use strict';

function showAlert(text) {
    // alert(text)
    console.log(text)
}

function listFormElement() {
    var formEle = document.getElementsByTagName('form');
    console.log(formEle);
}

function copyTextFromNameToLastName() {
    var fnameEle = document.getElementById('fname');
    var lnameEle = document.getElementById('lname');
    var fname = fnameEle.value;

    console.log('First name is ' + fname);
    console.log('Last name is ' + lnameEle.value);
    lnameEle.value = fname;
}

function reset1() {
    var fnameEle = document.getElementById('fname');
    var lnameEle = document.getElementById('lname');
    var mailEle = document.getElementById('email');
    var passEle = document.getElementById('password');
    var cpassEle = document.getElementById('cpassword');
    var rad1Ele = document.getElementById('inlineRadio1');
    var rad2Ele = document.getElementById('inlineRadio2');
    var checkEle = document.getElementById('customCheck1');

    fnameEle.value = '';
    lnameEle.value = '';
    mailEle.value = '';
    passEle.value = '';
    cpassEle.value = '';
    rad1Ele.checked = false;
    rad2Ele.checked = false;
    checkEle.checked = false;
}

function getRadioValue() {
    var rad1Ele = document.getElementById('inlineRadio1');
    var rad2Ele = document.getElementById('inlineRadio2');

    if (rad1Ele.checked == true) {
        return rad1Ele.value;
    } else if (rad2Ele.checked == true) {
        return rad2Ele.value;
    } else {
        return 'not selected';
    }
}

function showSummary() {

    var fnameEle = document.getElementById('fname').value;
    var lnameEle = document.getElementById('lname').value;
    var mailEle = document.getElementById('email').value;
    var passEle = document.getElementById('password').value;
    var cpassEle = document.getElementById('cpassword').value;

    var sumEle = document.getElementById('summary');

    if (fnameEle == '' && lnameEle == '' && mailEle == '' && passEle == '' && cpassEle == '') {
        showAlert('Form is empty');
        sumEle.setAttribute('hidden', true);

    } else {
        sumEle.innerHTML = "<p>Name : " + fnameEle + " " + lnameEle +
            "<br> Email : " + mailEle +
            "<br>Password : " + passEle +
            "<br>Confirm Password : " + cpassEle +
            "<br>Status : " +
            getRadioValue() + "</p>";
        sumEle.removeAttribute('hidden');
    }
}

function changeToEmail() {
    var rad1Ele = document.getElementById('inlineRadio1');
    var rad2Ele = document.getElementById('inlineRadio2');
    var mailEle = document.getElementById('email');

    if (rad1Ele.checked == true) {
        mailEle.value = rad1Ele.value;
    } else if (rad2Ele.checked == true) {
        mailEle.value = rad2Ele.value;
    } else {
        console.log('Plz select')
    }
}

function addPElement() {
    var node = document.createElement('p');
    node.innerHTML = 'Hello world';
    var node2 = document.createElement('a');
    node2.innerHTML = 'Google';
    node2.setAttribute('href', 'https://www.google.com')
    node2.setAttribute('target', '_blank')
    var placeHolder = document.getElementById('placeholder');
    placeHolder.appendChild(node);

    placeHolder.appendChild(node2);
}

function addForm() {
    var node = document.createElement('p');
    node.innerHTML = 'Please fill in this form to create account';
    var placeHolder = document.getElementById('pFill');
    placeHolder.appendChild(node);

    var node2 = document.createElement('input');
    node2.setAttribute('type', 'text')
    node2.setAttribute('class', 'form-control')
    node2.setAttribute('id', 'fname')
    node2.setAttribute('placeholder', 'First name')
    var placeHolder1 = document.getElementById('firstForm');
    placeHolder1.appendChild(node2);

    var node3 = document.createElement('input');
    node3.setAttribute('type', 'text')
    node3.setAttribute('class', 'form-control')
    node3.setAttribute('id', 'lname')
    node3.setAttribute('placeholder', 'Last name')
    var placeHolder2 = document.getElementById('secondForm');
    placeHolder2.appendChild(node3);

    var node4 = document.createElement('input');
    node4.setAttribute('type', 'email')
    node4.setAttribute('class', 'form-control')
    node4.setAttribute('id', 'email')
    node4.setAttribute('placeholder', 'Enter email')
    node4.setAttribute('aria-describedby', 'emailHelp')
    var placeHolder3 = document.getElementById('thirdForm');
    placeHolder3.appendChild(node4);
}

function addTable() {
    var table = document.createElement('table')
    table.setAttribute('class', 'table table-striped')

    var thead = document.createElement('thead')
    var node3 = document.createElement('tr')
    var node4 = document.createElement('th')
    node4.setAttribute('scope', 'col')
    node4.innerHTML = '#'
    var node5 = document.createElement('th')
    node5.setAttribute('scope', 'col')
    node5.innerHTML = 'First'
    var node6 = document.createElement('th')
    node6.setAttribute('scope', 'col')
    node6.innerHTML = 'Last'
    var node7 = document.createElement('th')
    node7.setAttribute('scope', 'col')
    node7.innerHTML = 'Handle'

    var tbody = document.createElement('tbody')
    var tr = document.createElement('tr')
    var th = document.createElement('th')
    th.setAttribute('scope', 'row')
    th.innerHTML = '1'
    var td1 = document.createElement('td')
    td1.innerHTML = 'Mark'
    var td2 = document.createElement('td')
    td2.innerHTML = 'Otto'
    var td3 = document.createElement('td')
    td3.innerHTML = '@mdo'

    var tr2 = document.createElement('tr')
    var th2 = document.createElement('th')
    th2.setAttribute('scope', 'row')
    th2.innerHTML = '2'
    var td21 = document.createElement('td')
    td21.innerHTML = 'Jacob'
    var td22 = document.createElement('td')
    td22.innerHTML = 'Thornton'
    var td23 = document.createElement('td')
    td23.innerHTML = '@fat'

    var tr3 = document.createElement('tr')
    var th3 = document.createElement('th')
    th3.setAttribute('scope', 'row')
    th3.innerHTML = '2'
    var td31 = document.createElement('td')
    td31.innerHTML = 'Jacob'
    var td32 = document.createElement('td')
    td32.innerHTML = 'Thornton'
    var td33 = document.createElement('td')
    td33.innerHTML = '@fat'

    table.appendChild(thead)
    thead.appendChild(node3)
    node3.appendChild(node4)
    node3.appendChild(node5)
    node3.appendChild(node6)
    node3.appendChild(node7)

    table.appendChild(tbody)
    tbody.appendChild(tr)
    tr.appendChild(th)
    tr.appendChild(td1)
    tr.appendChild(td2)
    tr.appendChild(td3)

    tbody.appendChild(tr2)
    tr2.appendChild(th2)
    tr2.appendChild(td21)
    tr2.appendChild(td22)
    tr2.appendChild(td23)

    tbody.appendChild(tr3)
    tr3.appendChild(th3)
    tr3.appendChild(td31)
    tr3.appendChild(td32)
    tr3.appendChild(td33)

    var tableSec = document.getElementById('tableSection')
    tableSec.appendChild(table)
}

function genTable() {
    var num = document.getElementById('number').value
    console.log(num)

    var table = document.createElement('table')
    table.setAttribute('class', 'table')

    var thead = document.createElement('thead')
    var tr = document.createElement('tr')
    var th = document.createElement('th')
    th.setAttribute('scope', 'col')
    th.innerHTML = '#'

    var tbody = document.createElement('tbody')

    table.appendChild(thead)
    thead.appendChild(tr)
    tr.appendChild(th)
    table.appendChild(tbody)

    for (var i = 1; i <= num; i++) {
        var tr1 = document.createElement('tr')
        var td1 = document.createElement('td')
        td1.setAttribute('scope', 'row')
        td1.innerHTML = i

        tbody.appendChild(tr1)
        tr1.appendChild(td1)
    }

    var tableGenSec = document.getElementById('tableGenSection')
    tableGenSec.firstChild.remove()
    tableGenSec.appendChild(table)

}